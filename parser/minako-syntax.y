%define parse.error verbose
%define parse.trace

%code requires {
	#include <stdio.h>
	#include <stdlib.h>

	extern void yyerror(const char*);
	extern FILE *yyin;
}

%code {
	extern int yylex();
	extern int yylineno;
}

%union {
	char *string;
	double floatValue;
	int intValue;
}

%token AND           "&&"
%token OR            "||"
%token EQ            "=="
%token NEQ           "!="
%token LEQ           "<="
%token GEQ           ">="
%token LSS           "<"
%token GRT           ">"
%token KW_BOOLEAN    "bool"
%token KW_DO         "do"
%token KW_ELSE       "else"
%token KW_FLOAT      "float"
%token KW_FOR        "for"
%token KW_IF         "if"
%token KW_INT        "int"
%token KW_PRINTF     "printf"
%token KW_RETURN     "return"
%token KW_VOID       "void"
%token KW_WHILE      "while"
%token CONST_INT     "integer literal"
%token CONST_FLOAT   "float literal"
%token CONST_BOOLEAN "boolean literal"
%token CONST_STRING  "string literal"
%token ID            "identifier"

%right "then"
%right KW_ELSE

%%

S:                      program;

program:                %empty |
                        program declassignment ';' |
                        program function_def;

function_def:           type id '(' para ')' '{' statementlist '}';

para:                   %empty |
                        parameterlist;

/* rekursiv */
parameterlist_r:        %empty |
                        parameterlist_r ',' type id;

parameterlist:          type id parameterlist_r;

functioncall:           id '(' assignments_r ')';

type:                   KW_BOOLEAN |
                        KW_FLOAT |
                        KW_INT |
                        KW_VOID;

assignments_r:          %empty |
                        assignment assignments_c;

/* rekursiv */
assignments_c:          %empty |
                        assignments_c ',' assignment;

statementlist:          statementlist_blocks;

statementlist_blocks:   %empty |
                        statementlist_blocks block;

block:                  '{' statementlist '}' |
                        statement;

statement:              ifstatement |
                        forstatement |
                        whilestatement |
                        returnstatement ';' |
                        dowhilestatement ';' |
                        printf ';' |
                        declassignment ';' |
                        statassignment ';' |
                        functioncall ';';

statblock:              '{' statementlist '}' |
                        statement;

ifstatement:            KW_IF '(' assignment ')' statblock %prec "then"|
                        KW_IF '(' assignment ')' statblock KW_ELSE statblock %prec KW_ELSE;

forstatement:           KW_FOR '(' statassignment ';' expression ';' statassignment ')' statblock |
                        KW_FOR '(' declassignment ';' expression ';' statassignment ')' statblock;

dowhilestatement:       KW_DO statblock KW_WHILE '(' assignment ')';

whilestatement:         KW_WHILE '(' assignment ')' statblock;

returnstatement:        KW_RETURN assignment;

expression:             simpexpr comparison;

printf:                 KW_PRINTF '(' assignment ')' |
                        KW_PRINTF '(' CONST_STRING ')';

declassignment:         type id eq;

/* keine Zuweising oder mit Zuweisung */
eq:                     %empty |
                        '=' assignment;

statassignment:         id '=' assignment;

assignment:             id '=' assignment |
                        expression;

comparison:             %empty |
                        EQ simpexpr |
                        NEQ simpexpr |
                        LEQ simpexpr |
                        GEQ simpexpr |
                        LSS simpexpr |
                        GRT simpexpr;

simpexpr:               '-' term simpexpr_l |
                        term simpexpr_l;

/* left */
simpexpr_l:             %empty |
                        simpexpr_l '+' term |
                        simpexpr_l '-' term |
                        simpexpr_l OR term;

term:                   factor term_c;

term_c:                 %empty |
                        term_c '*' factor |
                        term_c '/' factor |
                        term_c AND factor|
                        term_c OR factor;

factor:                 CONST_INT |
                        CONST_FLOAT |
                        CONST_BOOLEAN |
                        functioncall |
                        id |
                        '(' assignment ')';

id:                     ID

%%

int main(int argc, char *argv[]) {
	yydebug = 0;

	if (argc != 2) {
            yyin = stdin;
        } else {
            yyin = fopen(argv[1], "r");
            if (yyin == 0) {
                fprintf(
                        stderr,
                        "Fehler: Konnte Datei %s nicht zum lesen oeffnen.\n",
                        argv[1]
                );
                exit(-1);
            }
        }

	return yyparse();
}

void yyerror(const char *msg) {
	fprintf(stderr, "%s\n", msg);
	exit(-1);
}
